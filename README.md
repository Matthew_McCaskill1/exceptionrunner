# Lab 8: Exception Tester

Answers to questions:  
  
1. The output in `logger.log` is different from the console output because, in the `logger.properties` file, the console handler level is set to `INFO`, while the `logger.log` handler level is set to `ALL`, thus, the log file also has the `FINER` level output.  
2. This line comes from the logging present in the JUnit library. Specifically in the `org.junit.jupiter.engine.execution.ConditionEvaluator` class.  
3. `Assertions.assertThrows` asserts that the specified exception is expected to be thrown. If it was not thrown, the test will fail.  
4. `Timer` class:
   1. The `Throwable` base class implements the `Serializable` interface, which causes this field to be generated automatically. However, we can manually specify the number as well, which is why it is present. The purpose of the field is so that the Java serializer and deserializer can confirm compatability between the stored data and the programmatic interface.
   2. We need to override the constuctors so that exception chaining works properly. Since constructors are *not* inherited when defining a subtype, we need to manually add these constructors.
   3. Similar to 4.2, we do *not* need to override the rest of the `Exception` methods because those *are* inherited with defining a subtype and, thus, are accessable using the subtype's interface.
5. The `static` block in `Timer` does static initialization of the logger for the class. In other words, it initializes static variables. This occurs when the `static` `timeMe` method is called, before the method body is run.
6. The file format of `README.md` is **M**ark**d**own (hence the `.md` extension). Markdown files are (usually) automattically converted into HTML (akin to LaTeX being converted into a PDF) by the system using them. In the case of BitBucket and GitHub, among others, any Markdown files are automatticaly rendered as HTML when viewing them. The `README` file in particular is displayed as the "home page" of the repository.
7. In the `finally` block in `Timer#timeMe`, the `timeNow` variable is still `null` when it is used. To fix the bug, the statements in the `finally` block should be moved into the `try` block. This will ensure that a thrown `TimerException` causes that those particular statements of the code to not be run when they shouldn't be.
8. The actual issue here is that the `finally` block is _always_ executed, regardless of whether an exception was caught or not. In this case, the `try` block threw a `TimerException`. This was not covered by the `catch` block, so the `finally` block occurs before exiting the method due to the exception. However, within the `finally` block, a `NullPointerException` was thrown, causing this exception to be thrown instead. So, the test failed not because an exception wasn't thrown, but because the wrong exception was thrown.
9. ![JUnit Plugin](./screenshots/plugin.png)
10. ![Maven Tests](./screenshots/maven.png)
11. `TimerException` is a _checked_ exception (as it inherits from base class `Exception`). `NullPointerException` is a _checked_ exception (as it inherits from base class `RuntimeException`).
12. [Repository Link: https://bitbucket.org/Matthew_McCaskill1/exceptionrunner/src/master/](https://bitbucket.org/Matthew_McCaskill1/exceptionrunner/src/master/)
